Imports System.IO
Imports System.Text
Imports System.Configuration.ConfigurationSettings
Imports ntb_FuncLib

Public Class Spill
    'public variable defines if object should be written to file
    Public s_write As Boolean = False

    'declare global variables
    Private s_id As Integer
    Private s_name As String
    Private s_counter As Integer
    Private s_track As String

    Private strHtml As String

    'class init
    Public Sub New(ByVal name As String, ByVal track As String, ByVal racedate As String)
        s_counter = 1 'reset counter
        s_name = name 'spill navn
        s_track = track 'bane navn
        'strHtml = "<html>" & vbCrLf & "<body>" & vbCrLf ' set html start tags
        strHtml = HtmlHead(s_name & "_" & racedate)
        strHtml += "<p class=Infolinje>" & s_name & " " & s_track & " " & racedate & "</p><p class=brdtekst><b>Vurdert av (navn), NTB.</b><BR><BR></p>"
    End Sub

    'add html table string
    Public Sub AddTable(ByVal tab As String)
        strHtml += tab
        SetTableFooter()
        'if table is added once or more then write to file
        s_write = True
    End Sub

    'end html string
    Public Sub EndHTML()
        Select Case s_name
            Case "DD"
                strHtml += DDBottom()
            Case "V65"
                strHtml += DDBottom()
            Case "V75"
                strHtml += V75Bottom()
        End Select
        strHtml += "</body>" & vbCrLf & "</html>" 'set html end tags
    End Sub

    'return html string
    Public Function GetHTML() As String
        Return strHtml
    End Function

    'set html table header 
    Public Sub SetTableHeader(ByVal raceno As String, ByVal dist As String, ByVal start As String)
        Dim ov, tab As String
        Select Case s_name
            Case "DD"
                'ov = "[ddov]"
                'tab = "[ddtab]"
                strHtml += "<p class=brdtekst><font color=""red"">[ddov]</font><BR>"
                strHtml += "<b>" & s_counter & ". l�p " & dist & " meter " & start & "</b><BR>"
                strHtml += "<font color=""red"">[ddtab]</font><BR></p>"
            Case "V65"
                'ov = "[ddov]"
                'tab = "[ddtab]"
                strHtml += "<p class=brdtekst><font color=""red"">[ddov]</font><BR>"
                strHtml += "<b>" & s_counter & ". avd " & dist & " meter " & start & "</b><BR>"
                strHtml += "<font color=""red"">[ddtab]</font><BR></p>"
            Case "V75"
                'ov = "[v75ov]"
                'tab = "[v75tab]"
                strHtml += "<p class=brdtekst><font color=""red"">[v75ov]</font><BR>"
                strHtml += "<b>" & s_counter & ". l�p " & dist & " meter " & start & "</b><BR>"
                strHtml += "<font color=""red"">[v75tab]</font><BR></p>"
            Case Else
                strHtml += "<p class=brdtekst><font color=""red"">[ddov]</font><BR>"
                strHtml += "<b>" & s_counter & ". l�p " & dist & " meter " & start & "</b><BR>"
                strHtml += "<font color=""red"">[ddtab]</font><BR></p>"
        End Select
        'strHtml += "<font color=""red"">" & ov & "</font><BR>"
        'strHtml += "<b>" & s_counter & ". l�p " & dist & " meter " & start & "</b><BR>"
        'strHtml += "<font color=""red"">" & tab & "</font><BR>"
        s_counter += 1 'incerement counter
    End Sub

    'set html table footer
    Private Sub SetTableFooter()
        Dim tip As String
        Select Case s_name
            Case "DD"
                'tip = "[ddtip]"
                strHtml += "<p class=brdtekst><BR><font color=""red"">[ddtip]"
                strHtml += "<BR>{bi}</font>(HEST1) - (HEST 2) - (HEST 3).  OUTS.: (OUTSIDERHEST)"
                strHtml += "<BR><font color=""red"">{bt}</font>(Sett inn kommentarene her.)<BR><BR></p>"
            Case "V65"
                'tip = "[v65tip]"
                strHtml += "<p class=brdtekst><BR><font color=""red"">[ddtip]"
                strHtml += "<BR>{bi}</font>(HEST1) - (HEST 2) - (HEST 3).  OUTS.: (OUTSIDERHEST)"
                strHtml += "<BR><font color=""red"">{bt}</font>(Sett inn kommentarene her.)<BR><BR></p>"
            Case "V75"
                'tip = "[v75tip]"
                strHtml += "<p class=brdtekst><BR><font color=""red"">[v75tip]"
                strHtml += "<BR>{bi}</font>(HEST1) - (HEST 2) - (HEST 3).  OUTS.: (OUTSIDERHEST)"
                strHtml += "<BR>Rangering: A: B: C:"
                strHtml += "<BR><font color=""red"">[v75kom]</font>"
                strHtml += "<BR>(Sett inn kommentarene her.)<BR><BR></p>"
            Case Else
                'tip = "[ddtip]"
                strHtml += "<p class=brdtekst><BR><font color=""red"">[ddtip]"
                strHtml += "<BR>{bi}</font>(HEST1) - (HEST 2) - (HEST 3).  OUTS.: (OUTSIDERHEST)"
                strHtml += "<BR><font color=""red"">{bt}</font>(Sett inn kommentarene her.)<BR><BR></p>"
        End Select
        'strHtml += "<BR><font color=""red"">" & tip
        'strHtml += "<BR>{bi}</font>(HEST1) - (HEST 2) - (HEST 3).  OUTS.: (OUTSIDERHEST)"
        'strHtml += "<BR><font color=""red"">{bt}</font>(Sett inn kommentarene her.)<BR><BR>"
    End Sub

    Public Sub ResetCounter()
        s_counter = 1
    End Sub


    Public Function GetName() As String
        Return s_name
    End Function

    'the html header
    Public Function HtmlHead(ByVal stikkord As String) As String
        Dim header, distkode, stoffgruppe As String

        distkode = "Nyhetstjenesten (ALL)"

        'select right distribution kode for current game
        If s_name.Equals("V75") Then
            distkode = "Tips/spill: Rikstoto (TOT)"
            stoffgruppe = "V75"
        Else
            distkode = "Tips/spill: Dagens Dobbel (DDO)"
            stoffgruppe = "V5/DD"
        End If

        header = "<html xmlns:o=""urn:schemas-microsoft-com:office:office""" & vbCrLf
        header += "xmlns:w=""urn:schemas-microsoft-com:office:word""" & vbCrLf
        header += "xmlns:dt=""uuid:C2F41010-65B3-11d1-A29F-00AA00C14882""" & vbCrLf
        header += "xmlns:st1=""urn:schemas-microsoft-com:office:smarttags""" & vbCrLf
        header += "xmlns=""http://www.w3.org/TR/REC-html40"">" & vbCrLf

        header += "<head>" & vbCrLf
        header += "<meta http-equiv=Content-Type content=""text/html; charset=windows-1252"">" & vbCrLf
        header += "<meta name=ProgId content=Word.Document>" & vbCrLf
        header += "<meta name=Generator content=""Microsoft Word 11"">" & vbCrLf
        header += "<meta name=Originator content=""Microsoft Word 11"">" & vbCrLf
        header += "<o:SmartTagType namespaceuri=""urn:schemas-microsoft-com:office:smarttags"" name=""place""/>" & vbCrLf

        header += "<!--[if gte mso 9]><xml>" & vbCrLf
        header += "<o:DocumentProperties>" & vbCrLf
        header += "<o:Author>Terje</o:Author>" & vbCrLf
        header += "<o:LastAuthor>Terje</o:LastAuthor>" & vbCrLf
        header += "<o:Company>NTB</o:Company>" & vbCrLf
        header += "</o:DocumentProperties>" & vbCrLf

        header += "<o:CustomDocumentProperties>" & vbCrLf
        header += "<o:NTBPrioritet dt:dt=""float"">5</o:NTBPrioritet>" & vbCrLf
        header += "<o:NTBMeldingsSign dt:dt=""string"">dnt/</o:NTBMeldingsSign>" & vbCrLf
        header += "<o:NTBTjeneste dt:dt=""string"">Spesialtjenester</o:NTBTjeneste>" & vbCrLf
        header += "<o:NTBStoffgruppe dt:dt=""string"">" & stoffgruppe & "</o:NTBStoffgruppe>" & vbCrLf
        header += "<o:NTBUndergruppe dt:dt=""string"">Nyheter</o:NTBUndergruppe>" & vbCrLf
        header += "<o:NTBHovedkategorier dt:dt=""string"">Sport</o:NTBHovedkategorier>" & vbCrLf
        header += "<o:NTBStikkord dt:dt=""string"">" & stikkord & "</o:NTBStikkord>" & vbCrLf
        header += "<o:NTBUnderKatSport dt:dt=""string"">Trav</o:NTBUnderKatSport>" & vbCrLf
        header += "<o:NTBDistribusjon dt:dt=""string"">" & distkode & "</o:NTBDistribusjon>" & vbCrLf
        header += "<o:NTBKanal dt:dt=""string"">A</o:NTBKanal>" & vbCrLf
        header += "<o:foldername dt:dt=""string"">NOTABENE\TravService</o:foldername>" & vbCrLf
        header += "<o:NTBMeldingsType dt:dt=""string"">Sportsresultater</o:foldername>" & vbCrLf
        header += "</o:CustomDocumentProperties>" & vbCrLf


        header += "</xml><![endif]--><!--[if gte mso 9]><xml>" & vbCrLf
        header += "<w:WordDocument>" & vbCrLf
        header += "<w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>" & vbCrLf
        header += "<w:IgnoreMixedContent>false</w:IgnoreMixedContent>" & vbCrLf
        header += "<w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>" & vbCrLf
        header += "<w:BrowserLevel>MicrosoftInternetExplorer4</w:BrowserLevel>" & vbCrLf
        header += "</w:WordDocument>" & vbCrLf
        header += "</xml><![endif]-->" & vbCrLf
        header += "<style>" & vbCrLf
        header += "p.Infolinje, li.Infolinje, div.Infolinje" & vbCrLf
        header += "{mso-style-name:_Infolinje;}" & vbCrLf
        header += "p.Brdtekst, li.Brdtekst, div.Brdtekst" & vbCrLf
        header += "{mso-style-name:_Br\00F8dtekst;}" & vbCrLf
        header += "p.Tabellkode, li.Tabellkode, div.Tabellkode" & vbCrLf
        header += "{mso-style-name:_Tabellkode;}" & vbCrLf
        header += "</style>" & vbCrLf
        header += "</head>" & vbCrLf

        Return header



    End Function
    'V75 end of html document
    Public Function V75Bottom() As String
        Dim strV75 As String

        strV75 = "<p class=brdtekst>" & vbCrLf
        strV75 += "<font color=red>[v75vur]</font><BR>" & vbCrLf
        strV75 += "<font color=red>{bi}</font>UKENS BANKERE:<BR>" & vbCrLf
        strV75 += "<font color=red>{bt}[v75kom]</font><BR>" & vbCrLf
        strV75 += "(Her skrives kommentaren til ukens bankere.)<BR><BR></p>" & vbCrLf

        strV75 += "<p class=brdtekst><font color=red>[v75vur]</font><BR>" & vbCrLf
        strV75 += "<font color=red>{bi}</font>FAVORITT I FARE: (Hestens navn her)<BR>" & vbCrLf
        strV75 += "<font color=red>{bt}[v75kom]</font><BR>" & vbCrLf
        strV75 += "(Her skrives kommentaren til favoritt i fare.)<BR><BR></p>" & vbCrLf

        strV75 += "<p class=brdtekst><font color=red>[v75fo]</font><BR>" & vbCrLf
        strV75 += "<font color=red>{bi}</font>STORT V75-FORSLAG: 576 REKKER = 288 KRONER.<BR>" & vbCrLf
        strV75 += "<font color=red>{bt}[v75tab]</font><BR></p>" & vbCrLf

        strV75 += "<p class=brdtekst><table border=1 class=Tabellkode>" & vbCrLf
        strV75 += "<tr><td>1</td><td>(forslag til f�rste l�p)</td></tr>" & vbCrLf
        strV75 += "<tr><td>2</td><td>(forslag til andre l�p)</td></tr>" & vbCrLf
        strV75 += "<tr><td>3</td><td>(forslag til tredje l�p)</td></tr>" & vbCrLf
        strV75 += "<tr><td>4</td><td>(forslag til fjerde l�p)</td></tr>" & vbCrLf
        strV75 += "<tr><td>5</td><td>(forslag til femte l�p)</td></tr>" & vbCrLf
        strV75 += "<tr><td>6</td><td>(forslag til sjette l�p)</td></tr" & vbCrLf
        strV75 += "<tr><td>7</td><td>(forslag til syvende l�p)</td></tr>" & vbCrLf
        strV75 += "</table><br></p>" & vbCrLf

        strV75 += "<p class=brdtekst><font color=red>[v75fo]</font><BR>" & vbCrLf
        strV75 += "<font color=red>{bi}</font>LITE V75-FORSLAG: 144 REKKER = 72 KRONER.<BR>" & vbCrLf
        strV75 += "<font color=red>{bt}[v75tab]</font><BR>" & vbCrLf
        strV75 += "<table border=1 class=Tabellkode>" & vbCrLf
        strV75 += "<tr><td>1</td><td>(forslag til f�rste l�p)</td></tr>" & vbCrLf
        strV75 += "<tr><td>2</td><td>(forslag til andre l�p)</td></tr>" & vbCrLf
        strV75 += "<tr><td>3</td><td>(forslag til tredje l�p)</td></tr>" & vbCrLf
        strV75 += "<tr><td>4</td><td>(forslag til fjerde l�p)</td></tr>" & vbCrLf
        strV75 += "<tr><td>5</td><td>(forslag til femte l�p)</td></tr>" & vbCrLf
        strV75 += "<tr><td>6</td><td>(forslag til sjette l�p)</td></tr>" & vbCrLf
        strV75 += "<tr><td>7</td><td>(forslag til syvende l�p)</td></tr>" & vbCrLf
        strV75 += "</table><br></p>" & vbCrLf

        strV75 += "<p class=brdtekst><font color=red>[v75kst]</font><br>" & vbCrLf
        strV75 += "<font color=red>{bi}</font>KUSKESTATISTIKK FOR (travbanens navn) (Pr. dagens dato)<br>" & vbCrLf
        strV75 += "<font color=red>{bt}[v75ks]</font>" & vbCrLf
        strV75 += "<table border=1 class=Tabellkode>" & vbCrLf
        strV75 += "<tr><td>1</td><td>(kuskens navn)</td><td>(ant. 1. plasser)</td><td>(ant. 2. plasser)</td><td>(innkj�rt kr.)</td></tr>" & vbCrLf
        strV75 += "<tr><td>2</td><td>(kuskens navn)</td><td>(ant. 1. plasser)</td><td>(ant. 2. plasser)</td><td>(innkj�rt kr.)</td></tr>" & vbCrLf
        strV75 += "<tr><td>3</td><td>(kuskens navn)</td><td>(ant. 1. plasser)</td><td>(ant. 2. plasser)</td><td>(innkj�rt kr.)</td></tr>" & vbCrLf
        strV75 += "<tr><td>4</td><td>(kuskens navn)</td><td>(ant. 1. plasser)</td><td>(ant. 2. plasser)</td><td>(innkj�rt kr.)</td></tr>" & vbCrLf
        strV75 += "<tr><td>5</td><td>(kuskens navn)</td><td>(ant. 1. plasser)</td><td>(ant. 2. plasser)</td><td>(innkj�rt kr.)</td></tr>" & vbCrLf
        strV75 += "<tr><td>6</td><td>(kuskens navn)</td><td>(ant. 1. plasser)</td><td>(ant. 2. plasser)</td><td>(innkj�rt kr.)</td></tr>" & vbCrLf
        strV75 += "<tr><td>7</td><td>(kuskens navn)</td><td>(ant. 1. plasser)</td><td>(ant. 2. plasser)</td><td>(innkj�rt kr.)</td></tr>" & vbCrLf
        strV75 += "<tr><td>8</td><td>(kuskens navn)</td><td>(ant. 1. plasser)</td><td>(ant. 2. plasser)</td><td>(innkj�rt kr.)</td></tr>" & vbCrLf
        strV75 += "<tr><td>9</td><td>(kuskens navn)</td><td>(ant. 1. plasser)</td><td>(ant. 2. plasser)</td><td>(innkj�rt kr.)</td></tr>" & vbCrLf
        strV75 += "<tr><td>10</td><td>(kuskens navn)</td><td>(ant. 1. plasser)</td><td>(ant. 2. plasser)</td><td>(innkj�rt kr.)</td></tr>" & vbCrLf
        strV75 += "</table></p>" & vbCrLf
        'strV75 += "</p>" & vbCrLf

        Return strV75
    End Function

    Public Function DDBottom() As String
        Dim strDD As String = ""

        If s_name.Equals("V65") Then
            strDD = "<p class=brdtekst><font color=red>[ddforsl]</font><BR>" & vbCrLf
            strDD += "<font color=red>{bi}</font>STORT V65-FORSLAG:&nbsp;REKKER<BR>" & vbCrLf
            strDD += "<font color=red>{bt}</font>1.&nbsp;l�p:<BR>2.&nbsp;l�p:<BR><BR>" & vbCrLf
            strDD += "<font color=red>{bi}</font>LITE V65-FORSLAG:&nbsp;REKKER<BR>" & vbCrLf
            strDD += "<font color=red>{bt}</font>1.&nbsp;l�p:<BR>2.&nbsp;l�p:<BR><BR>" & vbCrLf
            strDD += "<font color=red>{bi}</font>BANKER:&nbsp;<BR>" & vbCrLf
            strDD += "<font color=red>{bt}</font>(Sett inn kommentar her.)<BR><BR></p>" & vbCrLf
        End If

        strDD += "<p class=brdtekst><font color=red>[ddforsl]</font><BR>" & vbCrLf
        strDD += "<font color=red>{bi}</font>STORT DD-FORSLAG:&nbsp;REKKER<BR>" & vbCrLf
        strDD += "<font color=red>{bt}</font>1.&nbsp;l�p:<BR>2.&nbsp;l�p:<BR><BR>" & vbCrLf
        strDD += "<font color=red>{bi}</font>LITE DD-FORSLAG:&nbsp;REKKER<BR>" & vbCrLf
        strDD += "<font color=red>{bt}</font>1.&nbsp;l�p:<BR>2.&nbsp;l�p:<BR><BR></p>" & vbCrLf

        Return strDD

    End Function
End Class


