'************************
'File: TravService.vb
'Date: 23.05.2005
'************************
Imports System.ServiceProcess
Imports System.Configuration.ConfigurationSettings
Imports ntb_FuncLib
Imports System.IO
Imports System.text
Imports System.Xml
Imports System.Web.Mail

Public Class Trav
    Inherits System.ServiceProcess.ServiceBase

#Region " Component Designer generated code "

    Public Sub New()
        MyBase.New()

        ' This call is required by the Component Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call

    End Sub

    'UserService overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    ' The main entry point for the process
    <MTAThread()> _
    Shared Sub Main()
        Dim ServicesToRun() As System.ServiceProcess.ServiceBase

        ' More than one NT Service may run within the same process. To add
        ' another service to this process, change the following line to
        ' create a second service object. For example,
        '
        '   ServicesToRun = New System.ServiceProcess.ServiceBase () {New Service1, New MySecondUserService}
        '
        ServicesToRun = New System.ServiceProcess.ServiceBase() {New Trav}

        System.ServiceProcess.ServiceBase.Run(ServicesToRun)
    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    ' NOTE: The following procedure is required by the Component Designer
    ' It can be modified using the Component Designer.  
    ' Do not modify it using the code editor.
    Friend WithEvents FileSystemWatcher As System.IO.FileSystemWatcher
    Friend WithEvents PollTimer As System.Timers.Timer
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.FileSystemWatcher = New System.IO.FileSystemWatcher
        Me.PollTimer = New System.Timers.Timer
        CType(Me.FileSystemWatcher, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PollTimer, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'FileSystemWatcher
        '
        Me.FileSystemWatcher.EnableRaisingEvents = True
        '
        'PollTimer
        '
        Me.PollTimer.Enabled = True
        Me.PollTimer.Interval = 1000
        '
        'Trav
        '
        Me.ServiceName = "TravService"
        CType(Me.FileSystemWatcher, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PollTimer, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub

#End Region

    'Declare Variables
    Private logPath As String
    Private inputPath As String
    Private outputPath As String
    Private donePath As String
    Private errorPath As String

    Protected IPTC_Sequence As Integer
    Protected IPTC_SequenceFile As String
    Protected IPTC_Range As String

    Private pollInterval As Integer
    Private fileTimer As Integer

    Private travresXSLT As String

    'Warnings
    Dim warnEmail As Boolean
    Dim emailRcpt As String
    Dim SMTPServer As String

    'Local Variables
    Private objSpill As Spill
    Private htSpill As New Hashtable 'tabell med spill objekter

    'Class initalisation
    '*******************
    Protected Sub Init()
        'Get global variables from configuration file
        logPath = AppSettings("LogPath")
        inputPath = AppSettings("InputPath")
        outputPath = AppSettings("OutputPath")
        donePath = AppSettings("DonePath")
        errorPath = AppSettings("ErrorPath")

        'Write to logfile
        LogFile.WriteLogNoDate(logPath, "---------------------------------------------------------------------------------")
        LogFile.WriteLog(logPath, "Trav Converter initiating...")

        pollInterval = AppSettings("PollingInterval")
        fileTimer = AppSettings("FileTimer")

        'Warning settings
        If (AppSettings("warnEmail") = "True") Then warnEmail = True Else warnEmail = False
        emailRcpt = AppSettings("emailRcpt")
        SMTPServer = AppSettings("SMTPServer")

        'skal ikke bruke IPTC?
        'IPTC_Range = AppSettings("IPTCRange")
        'IPTC_SequenceFile = AppSettings("IPTCSequenceFile")
        'IPTC_Sequence = CInt(IPTC_Range.Split("-")(0))
        'LoadSequence(IPTC_SequenceFile, True) 'read iptc number from file

        'create paths
        LogFile.MakePath(logPath)
        LogFile.MakePath(inputPath)
        LogFile.MakePath(outputPath)
        LogFile.MakePath(donePath)
        LogFile.MakePath(errorPath)

        'Get the XSLT sheet
        travresXSLT = AppSettings("TravResXSLT")

    End Sub

#Region "IPTC NOT USED"


    'This sub can either load the IPTC number from the text file or
    'save it to the textfile. True = load, False = save
    '*******************************************************************
    Private Sub LoadSequence(ByVal file As String, ByVal load As Boolean)
        Try
            If load Then
                IPTC_Sequence = LogFile.ReadFile(file).Trim()
                LogFile.WriteLog(logPath, "IPTC Sequence number loaded: " & IPTC_Sequence)
            Else
                LogFile.WriteFile(file, IPTC_Sequence, False)
                LogFile.WriteLog(logPath, "IPTC Sequence number saved: " & IPTC_Sequence)
            End If
        Catch ex As Exception
            LogFile.WriteErr(logPath, "Error accessing IPTC sequence number.", ex)
        End Try
    End Sub


    'Get Sequence Number, Increment and return
    '*****************************************
    Private Function Get_Seq() As Integer

        'Check value
        If IPTC_Sequence > CInt(IPTC_Range.Split("-")(1)) Or _
           IPTC_Sequence < CInt(IPTC_Range.Split("-")(0)) Then
            IPTC_Sequence = CInt(IPTC_Range.Split("-")(0))
        End If

        'Return value
        Dim ret As Integer = IPTC_Sequence

        'Increment
        IPTC_Sequence += 1

        'Save
        LoadSequence(IPTC_SequenceFile, False)

        Return ret
    End Function
#End Region
    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.
        Init()

        PollTimer.Enabled = True
        FileSystemWatcher.Path = inputPath
        FileSystemWatcher.EnableRaisingEvents = True

        LogFile.WriteLog(logPath, "Trav Converter started.")
    End Sub

    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
        PollTimer.Enabled = False
        FileSystemWatcher.EnableRaisingEvents = False

        'LoadSequence(IPTC_SequenceFile, False) 'NOT USING IPTC
        LogFile.WriteLog(logPath, "Trav Converter stopped.")
    End Sub

    'Loop through all files
    'in input path
    '**********************
    Private Sub DoAllFiles()

        'Wait for files to be written
        Threading.Thread.Sleep(fileTimer * 1000)

        Dim file As String
        Dim files As String() = Directory.GetFiles(inputPath)

        'Loop the files
        For Each file In files
            DoOneFile(file)
        Next
    End Sub

    'Go thorugh each file and convert it using appropriate class
    '***********************************************************
    Private Sub DoOneFile(ByVal filename As String)

        Dim ok As Boolean = True
        Dim oldfile As String = filename
        'Dim outfile As String = outputPath & "\" & Path.GetFileNameWithoutExtension(filename) & ".xml"
        Dim nitf As String
        Dim type As String
        Dim strXML, strTmp, msg As String
        Dim num As Integer

        Try
            'her skal du sette inn ny kode
            'get outer xml and remove xmlns from root element
            strXML = GetFileContents(filename)
            strXML = strXML.Replace("xmlns=""http://integrasjon.balder""", "")
            Dim doc As XmlDocument = New XmlDocument
            doc.LoadXml(strXML)
            'get input parameters
            Dim root As XmlElement = doc.DocumentElement
            strTmp = root.SelectSingleNode("./header/messagetype").InnerText
            If strTmp.Equals("raceinfo") Then
                RaceInfo(doc, filename)
            ElseIf strTmp.Equals("raceresult") Then
                ok = RaceResult(strXML, filename)
            End If
            If ok = False Then
                Throw New Exception("Error in Raceresult")
            End If
        Catch ex As Exception
            msg = ex.Message
            Try
                File.Copy(oldfile, errorPath & "\" & Path.GetFileName(oldfile), True)
                File.Delete(oldfile)
            Catch
            End Try

            LogFile.WriteErr(logPath, "Konvertering feilet " & type & ": '" & oldfile & "'", ex)
            ok = False
        End Try

        'Copy/delete file
        If ok Then
            'LogFile.WriteLog(logPath, "Trav dokument konvertert" & type & ": '" & filename & "'")

            Try
                Dim donefile As String = donePath & "\" & Path.GetFileName(oldfile)
                donefile = FuncLib.MakeSubDirDate(donefile, File.GetLastWriteTime(oldfile))

                File.Copy(oldfile, donefile, True)
                File.Delete(oldfile)

                LogFile.WriteLog(logPath, "Slettet: '" & oldfile & "'")
            Catch ex As Exception
                LogFile.WriteErr(logPath, "Sletting feilet: '" & oldfile & "'", ex)
            End Try
        End If

        If Not ok Then
            'Email
            If warnEmail Then
                WarnOnEmail(msg)
            End If
        End If

    End Sub

    Protected Overrides Sub OnShutdown()
        OnStop()
    End Sub


    'When timer has counted down, trigger events for timer
    '*************************************************************************************************************************
    Private Sub PollTimer_Elapsed(ByVal sender As Object, ByVal e As System.Timers.ElapsedEventArgs) Handles PollTimer.Elapsed
        'Stop events while handling files
        PollTimer.Enabled = False
        FileSystemWatcher.EnableRaisingEvents = False

        'Do waiting files
        DoAllFiles()

        'Restart events
        PollTimer.Interval = pollInterval * 1000
        PollTimer.Enabled = True
        FileSystemWatcher.EnableRaisingEvents = True
    End Sub


    'Reading file content
    Public Function GetFileContents(ByVal FullPath As String, Optional ByRef ErrInfo As String = "") As String

        Dim strContents As String
        Dim objReader As StreamReader
        Try

            objReader = New StreamReader(FullPath, System.Text.Encoding.Default)
            strContents = objReader.ReadToEnd()
            objReader.Close()
            Return strContents
        Catch ex As Exception
            LogFile.WriteErr(logPath, "Exception in GetFileContents.", ex)
        End Try
    End Function

    Private Sub FileSystemWatcher_Created(ByVal sender As Object, ByVal e As System.IO.FileSystemEventArgs) Handles FileSystemWatcher.Created
        'Stop events while handling files
        PollTimer.Enabled = False
        FileSystemWatcher.EnableRaisingEvents = False

        'Do waiting files
        DoAllFiles()

        'Restart events
        PollTimer.Enabled = True
        FileSystemWatcher.EnableRaisingEvents = True
    End Sub

    'parse xml file for raceinfo using DOM
    Public Sub RaceInfo(ByVal doc As XmlDocument, ByVal filename As String)
        Dim strXML, strRtf, outfile As String
        Dim strTrackname, strLegType, strRaceNo, strDate, strDist, strTmp, strData, strStart As String 'parameters
        Dim i As Integer
        Dim m_node, n_node As XmlNode
        Dim m_nodelist, n_nodelist As XmlNodeList
        Dim htmtab As New HTMLTable
        Dim startno, horse, driver, dist, tmpdist, place, besttime, sm As String
        Dim alLegs As New ArrayList
        'Try
        'get root element
        Dim root As XmlElement = doc.DocumentElement

        'get globale race information
        m_node = root.SelectSingleNode("./meeting/track/trackname/text")
        strTrackname = m_node.InnerText
        m_node = root.SelectSingleNode("./meeting/date")
        strDate = m_node.SelectSingleNode("./day").InnerText & "/" & m_node.SelectSingleNode("./month").InnerText & "/" & m_node.SelectSingleNode("./year").InnerText

        'create spill objects and put in hashtable
        htSpill.Clear()
        'htSpill.Add("V5", New Spill("V5", strTrackname, strDate))
        htSpill.Add("V65", New Spill("V65", strTrackname, strDate))
        htSpill.Add("V75", New Spill("V75", strTrackname, strDate))
        htSpill.Add("DD", New Spill("DD", strTrackname, strDate))

        'get nodelist of race elements
        m_nodelist = doc.SelectNodes("/raceinfo/meeting/races/race")

        'loop through each race element in nodelist
        For Each m_node In m_nodelist
            'clear tables and datas for each race
            alLegs.Clear()
            htmtab.ClearTable()
            n_nodelist = m_node.SelectNodes("./legtypes/legtype")

            'inner loop for legtypes (spill typer) pr race
            For Each n_node In n_nodelist
                strTmp = n_node.SelectSingleNode("./pool/code").InnerText
                'this line decides what legtype to use in each race - eks DD, V65
                If strTmp.Equals("V65") Or strTmp.Equals("DD") Or strTmp.Equals("V75") Then
                    alLegs.Add(strTmp)
                End If
            Next

            'if no legtypes then skip table creation
            If alLegs.Count > 0 Then
                'race data
                strRaceNo = m_node.SelectSingleNode("./raceno").InnerText
                strDist = m_node.SelectSingleNode("./basedist").InnerText
                If m_node.SelectSingleNode("./startmethod").HasChildNodes Then
                    sm = m_node.SelectSingleNode("./startmethod/code").InnerText
                Else
                    sm = ""
                End If
                If sm.Equals("V") Then
                    strStart = "voltestart"
                ElseIf sm.Equals("A") Then
                    strStart = "autostart"
                End If

                tmpdist = strDist 'temporary used for comaparing

                'go through through xml data and make html tabell
                htmtab.StartTable()
                n_nodelist = m_node.SelectNodes("./starts/start")
                For Each n_node In n_nodelist
                    If n_node.SelectSingleNode("./trotinfo").HasChildNodes Then
                        dist = n_node.SelectSingleNode("./trotinfo/distance").InnerText
                    Else
                        dist = ""
                    End If
                    startno = n_node.SelectSingleNode("./startno").InnerText
                    horse = n_node.SelectSingleNode("./horse/key/name").InnerText
                    driver = n_node.SelectSingleNode("./driver/firstname").InnerText
                    driver += " " & n_node.SelectSingleNode("./driver/lastname").InnerText
                    If n_node.SelectSingleNode("./horsestat/formrows").HasChildNodes Then
                        place = GetPlaces(n_node.SelectNodes("./horsestat/formrows/formrow"))
                    End If
                    If n_node.SelectSingleNode("./horsestat/total/records").HasChildNodes Then
                        besttime = GetBesttime(n_node.SelectNodes("./horsestat/total/records"), sm)
                    End If
                    If Not tmpdist.Equals(dist) Then
                        htmtab.EndTable()
                        htmtab.SubTable(dist)
                        tmpdist = dist
                    End If
                    htmtab.AddRow(startno, horse, driver, place, besttime)
                Next
                htmtab.EndTable()

                'add html table for each legtype found (spill type)
                For i = 0 To alLegs.Count - 1
                    objSpill = htSpill.Item(alLegs.Item(i)) ' get current object and values
                    objSpill.SetTableHeader(strRaceNo, strDist, strStart) 'set table header

                    If objSpill.GetName.Equals("V75") Then
                        strTmp = htmtab.GetV75 'V75 tabell
                    Else
                        strTmp = htmtab.GetTable 'get html string into tmp
                    End If

                    tmpdist = objSpill.GetName.ToLower 'put lower case into tmp string

                    If tmpdist.Equals("v5") Then
                        tmpdist = "dd"
                    End If

                    strTmp = strTmp.Replace("[dst]", "[" & tmpdist & "dst]")
                    strTmp = strTmp.Replace("[tab]", "[" & tmpdist & "tab]")
                    objSpill.AddTable(strTmp) 'insert html table from edited tmp string
                    htSpill.Remove(alLegs.Item(i)) 'remove old object from table
                    htSpill.Add(alLegs.Item(i), objSpill) 'add updated object to hashtable
                Next
            End If
        Next

        'get all spill objects and store to disk
        Dim en As IDictionaryEnumerator = htSpill.GetEnumerator
        While en.MoveNext
            objSpill = en.Value
            objSpill.EndHTML()
            If objSpill.s_write = True Then
                strData = objSpill.GetHTML
                'Dump to disk - write the output html to a doc file (readable by MS Word)
                outfile = outputPath & "\" & Path.GetFileNameWithoutExtension(filename) & "_" & objSpill.GetName & ".doc"
                Dim sw As StreamWriter = New StreamWriter(outfile, False, Encoding.GetEncoding("iso-8859-1"))
                sw.WriteLine(strData)
                sw.Close()
                LogFile.WriteLog(logPath, "Dokument skrevet : " & outfile)
            End If
        End While
        'empty hashtable
        htSpill.Clear()
        'Catch ex As Exception
        'LogFile.WriteErr(logPath, "Exception in Raceinfo.", ex)
        'Return False
        'End Try

        'Return True
    End Sub

    'parse result file using xsl sheet
    Public Function RaceResult(ByVal xmlstring As String, ByVal filename As String) As Boolean
        Dim html As String
        Dim outfile As String
        Try
            Dim conv As TravConverter = New TravConverter(travresXSLT)
            html = conv.ConvertTravToXML(xmlstring)

            'Dump to disk - write the output html to a doc file (readable by MS Word)
            outfile = outputPath & "\" & Path.GetFileNameWithoutExtension(filename) & ".doc"
            Dim sw As StreamWriter = New StreamWriter(outfile, False, Encoding.GetEncoding("iso-8859-1"))
            sw.WriteLine(html)
            sw.Close()
            LogFile.WriteLog(logPath, "Dokument skrevet : " & outfile)
            Return True
        Catch ex As Exception
            LogFile.WriteErr(logPath, "Exception in RaceResult", ex)
            Return False
        End Try
    End Function

    Public Function GetPlaces(ByVal ndlist As XmlNodeList) As String
        Dim node As XmlNode
        Dim place As String = ""
        Dim tmp As String = ""
        Dim ctr As Integer = 0

        For Each node In ndlist
            If ctr < 3 Then
                'place += node.SelectSingleNode("./displayplace").InnerText
                tmp = node.SelectSingleNode("./displayplace").InnerText
                If tmp.Equals("-") Then
                    place += node.SelectSingleNode("./displaykmtime").InnerText
                Else
                    place += tmp
                End If
                If ctr < 2 Then
                    place += "-"
                End If
            End If
            ctr += 1
        Next
        Return place
    End Function

    ' get the best time for current horse
    Public Function GetBesttime(ByVal ndlist As XmlNodeList, ByVal startmethod As String) As String
        Dim node_a, node_b As XmlNode
        Dim ndlist_b As XmlNodeList
        Dim bst As String

        For Each node_a In ndlist
            ndlist_b = node_a.SelectNodes("./record")
            For Each node_b In ndlist_b
                If node_b.Attributes("beststartmethod").InnerText.Equals("true") Then
                    If node_b.SelectSingleNode("./startmethod/code").InnerText.Equals(startmethod) Then
                        bst = node_b.SelectSingleNode("./time/second").InnerText
                        bst += "," & node_b.SelectSingleNode("./time/tenth").InnerText
                    End If
                End If
            Next
        Next
        Return bst
    End Function

    'Warns on connection errors by e-Mail
    Sub WarnOnEmail(ByVal message As String)
        Dim epost As MailMessage = New MailMessage
        SmtpMail.SmtpServer = SMTPServer

        epost.From = "505@ntb.no"
        epost.To = emailRcpt
        epost.Subject = "Feil i Travservice!"
        epost.BodyFormat = Web.Mail.MailFormat.Text
        epost.Body = "Det har oppst�tt en feil i Travservice." & vbCrLf & vbCrLf
        epost.Body = epost.Body & "Tid: " & Now & vbCrLf
        epost.Body = epost.Body & "Feilmelding: " & message & vbCrLf & vbCrLf
        'epost.Body = epost.Body & query
        Try
            SmtpMail.Send(epost)
        Catch e As Exception
            'Log warning error
            'systemLog.WriteLine(Now & " : FAILED to send warning by e-Mail!")
            'LogError(e)
            LogFile.WriteLog(logPath, "FAILED to send warning by e-Mail!")
            LogFile.WriteErr(logPath, "", e)
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub
End Class
