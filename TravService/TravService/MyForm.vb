Imports System.Configuration.ConfigurationSettings
Imports System.text
Imports System.IO
Imports ntb_FuncLib

Public Class MyForm
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.Button1 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(24, 80)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(168, 20)
        Me.TextBox2.TabIndex = 7
        Me.TextBox2.Text = ""
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(24, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(56, 23)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Filename:"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(88, 16)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(272, 20)
        Me.TextBox1.TabIndex = 5
        Me.TextBox1.Text = ""
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(24, 48)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(168, 23)
        Me.Button1.TabIndex = 4
        Me.Button1.Text = "Convert"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(216, 48)
        Me.Button2.Name = "Button2"
        Me.Button2.TabIndex = 8
        Me.Button2.Text = "Replace"
        '
        'MyForm
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(464, 213)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Button1)
        Me.Name = "MyForm"
        Me.Text = "MyForm"
        Me.ResumeLayout(False)

    End Sub

#End Region

    'Vars
    Private logPath As String
    Private inputPath As String
    Private outputPath As String
    Private donePath As String
    Private errorPath As String

    Protected IPTC_Sequence As Integer
    Protected IPTC_SequenceFile As String
    Protected IPTC_Range As String

    Private pollInterval As Integer

    Private plussXSLT As String

    Private Sub MyForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        logPath = AppSettings("LogPath")
        inputPath = AppSettings("InputPath")
        outputPath = AppSettings("OutputPath")
        donePath = AppSettings("DonePath")
        errorPath = AppSettings("ErrorPath")

        LogFile.MakePath(logPath)
        LogFile.MakePath(inputPath)
        LogFile.MakePath(outputPath)
        LogFile.MakePath(donePath)
        LogFile.MakePath(errorPath)

        IPTC_Range = AppSettings("IPTCRange")

        'Pluss' NITF specifics
        plussXSLT = AppSettings("TravResXSLT")

        'test
        Me.TextBox1.Text = "c:\Utvikling\trav\inn\test.xml"
    End Sub

    'Convert one file
    Private Sub DoOneFile(ByVal filename As String)

        Dim ok As Boolean = True
        Dim oldfile As String = filename
        Dim outfile As String = outputPath & "\" & Path.GetFileNameWithoutExtension(filename) & ".xml"
        Dim nitf As String
        Dim type As String
        Dim strTrav As String


        Try
            'open file and remove xmlns from root node
            strTrav = Me.GetFileContents(filename)
            strTrav = strTrav.Replace("xmlns=""http://integrasjon.balder""", "")


            'Convert file
            If filename.IndexOf(".xml") > -1 Then
                type = "(Pluss)"
                Dim conv As TravConverter = New TravConverter(plussXSLT)
                'nitf = conv.ConvertPlussFile2NITF(filename, Get_Seq())
                'nitf = conv.ConvertTravToXML(filename, 8300, strTrav)
                'ElseIf filename.IndexOf("\\na") > -1 Then
                '   type = "(NWA)"
                '  nitf = NWAConverter.ConvertNWAFile2NITF(filename, Get_Seq())
                'Else
                'type = "(BWI)"
                'nitf = BWConverter.ConvertBWFile2NITF(filename, Get_Seq())
                'nitf = BWConverter.ConvertBWFile2NITF(filename, 8300)
                'outfile = outputPath & "\" & filename & ".xml"
            End If

            'Dump to disk
            Dim sw As StreamWriter = New StreamWriter(outfile, False, Encoding.GetEncoding("iso-8859-1"))
            sw.WriteLine(nitf)
            sw.Close()

        Catch ex As Exception
            Try
                File.Copy(oldfile, errorPath & "\" & Path.GetFileName(oldfile), True)
                File.Delete(oldfile)
            Catch
            End Try

            LogFile.WriteErr(logPath, "Konvertering feilet " & type & ": '" & oldfile & "'", ex)
            ok = False
        End Try

        'Copy/delete file
        If ok Then
            'LogFile.WriteLog(logPath, "PRM dokument konvertert" & type & ": '" & filename & "'")

            Try
                Dim donefile As String = donePath & "\" & Path.GetFileName(oldfile)
                donefile = FuncLib.MakeSubDirDate(donefile, File.GetLastWriteTime(oldfile))

                File.Copy(oldfile, donefile, True)
                File.Delete(oldfile)

                LogFile.WriteLog(logPath, "Slettet: '" & oldfile & "'")
            Catch ex As Exception
                LogFile.WriteErr(logPath, "Sletting feilet: '" & oldfile & "'", ex)
            End Try
        End If

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim filename As String
        filename = Me.TextBox1.Text
        'filename = "C:\Temp\prm\inn\PRM-20050518_1560.xml"
        DoOneFile(filename)
    End Sub

    'Reading file content
    Public Function GetFileContents(ByVal FullPath As String, _
           Optional ByRef ErrInfo As String = "") As String

        Dim strContents As String
        Dim objReader As StreamReader
        Try

            objReader = New StreamReader(FullPath)
            strContents = objReader.ReadToEnd()
            objReader.Close()
            Return strContents
        Catch Ex As Exception
            ErrInfo = Ex.Message
        End Try
    End Function

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim strText As String
        Dim strReplace As String
        strReplace = """http://int.bald"""
        strText = Me.TextBox1.Text
        Me.TextBox2.Text = strText.Replace(strReplace, "")
    End Sub
End Class
