Imports System.IO
Imports System.Text
Imports System.Configuration.ConfigurationSettings
Imports ntb_FuncLib

Public Class HTMLTable
    'declare global variables
    Private strHTM, V75TAB As String
    Private logPath As String

    Public Sub New()
        'Get global variables from configuration file
        logPath = AppSettings("logPath")
    End Sub

    Public Sub AddRow(ByVal startnum As String, ByVal horse As String, ByVal driver As String, Optional ByVal cel1 As String = "", Optional ByVal cel2 As String = "")
        strHTM += "<tr><td>" & startnum & "</td><td>" & horse & ", " & driver & "</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>" & vbCrLf
        'strHTM += "<tr><td>" & startnum & "</td><td>" & horse & ", " & driver & "</td><td>" & cel1 & "</td><td>" & cel2 & "</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>" & vbCrLf
        V75TAB += "<tr><td>" & startnum & "</td><td>" & horse & ", " & driver & "</td><td>" & cel1 & "</td><td>" & cel2 & "</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>" & vbCrLf
    End Sub

    'start table tags
    Public Sub StartTable()
        strHTM += "<table border=1 class=Tabellkode>" & vbCrLf
        V75TAB += "<table border=1 class=Tabellkode>" & vbCrLf
    End Sub

    'end table tags
    Public Sub EndTable()
        strHTM += "</table>" & vbCrLf
        V75TAB += "</table>" & vbCrLf
    End Sub

    'creates a sub table for new distances
    Public Sub SubTable(ByVal dist As String)
        strHTM += "<p class=brdtekst><BR><font color=""red"">[dst]</font><BR></p>" & vbCrLf
        strHTM += "<p class=brdtekst><b>" & dist & " meter</b><BR><font color=""red"">[tab]</font><BR></p>" & vbCrLf
        V75TAB += "<p class=brdtekst><BR><font color=""red"">[dst]</font><BR></p>" & vbCrLf
        V75TAB += "<p class=brdtekst><b>" & dist & " meter</b><BR><font color=""red"">[tab]</font><BR></p>" & vbCrLf
        StartTable()
    End Sub

    'return html table string
    Public Function GetTable() As String
        Return strHTM
    End Function

    Public Function GetV75() As String
        Return V75TAB
    End Function

    'clear html table string
    Public Sub ClearTable()
        strHTM = ""
        V75TAB = ""
    End Sub

End Class
