Imports System.io

Public Class TravConverter
    Private xsltConverter As MSXML2.IXSLProcessor


    Public Sub New(ByVal convertXSLTFile As String)

        Dim xsltDoc As MSXML2.FreeThreadedDOMDocument
        Dim xmlError As MSXML2.IXMLDOMParseError
        Dim xslt As MSXML2.XSLTemplate

        xslt = New MSXML2.XSLTemplate
        xsltDoc = New MSXML2.FreeThreadedDOMDocument

        xsltDoc.load(convertXSLTFile)
        xmlError = xsltDoc.parseError

        xslt.stylesheet = xsltDoc
        xsltConverter = xslt.createProcessor

    End Sub

    Public Function ConvertTravToXML(ByVal strXML As String) As String
        Dim xml4Doc As MSXML2.DOMDocument = New MSXML2.DOMDocument

        'xml4Doc.load(file) - file variabel brukes ikke i denne lÝsningen
        xml4Doc.loadXML(strXML) ' bruker xml streng i stedet
        'xsltConverter.addParameter("filename", Path.GetFileName(file))
        'xsltConverter.addParameter("iptc_seq", iptc)
        xsltConverter.input = xml4Doc

        xsltConverter.transform()

        Return Replace(xsltConverter.output, "encoding=""UTF-16""", "encoding=""iso-8859-1""", 1, 1)

    End Function
End Class
